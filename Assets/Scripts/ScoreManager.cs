using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI Scoretext;
    public TextMeshProUGUI HighScore;
    public int score;
    int highscore;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        highscore = PlayerPrefs.GetInt("Highscore",0);
        //Scoretext.text = "" + score.ToString();
        HighScore.text = "Highscore :" +"\n"+ highscore.ToString();
        

    }
    void Update()
    {
        Scoretext.text =  score.ToString();
        PlayerPrefs.SetInt("Score", score); 
    }

    // Update is called once per frame
    public void tambahscore()
    {
        score += 100;
        if (highscore < score )
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }

    /*public void tambahscorebeam()
    {
        score += 500;
        Scoretext.text = "" + score.ToString();
        PlayerPrefs.SetInt("scoreuntuklevel", score);
        if (highscore < score)
        {
            PlayerPrefs.SetInt("Highscore", score);
        }
    }*/

  
}

    
