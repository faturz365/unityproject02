using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KodinganUntukSpawn : MonoBehaviour
{
    public Transform[] m_spawnpoints;
    public GameObject[] m_enemyPrefabs;
    public float m_BesarObjek = 0.1f;
    public ScoreManager score;

    // Start is called before the first frame update
    void Start()
    {
        string _namascenesaatini = SceneManager.GetActiveScene().name;
        if (_namascenesaatini == "Main")
        {
            score = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
            StopCoroutine(e_spawnmenu());
        }
        else if (_namascenesaatini == "Menu")
        {
            StartCoroutine(e_spawnmenu());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   public IEnumerator e_spawnenemy()
    {
        while (true)
        {
            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length);
            int _Type = 0;
            if (score.score >= 0 && score.score <= 2000) 
            {
                 _Type = Random.Range(0, m_enemyPrefabs.Length - 3);
            }
            else if (score.score > 2000 && score.score <= 4000)
            {
                 _Type = Random.Range(0, m_enemyPrefabs.Length - 2);
            }
            else if (score.score > 4000 && score.score <= 10000)
            {
                 _Type = Random.Range(0, m_enemyPrefabs.Length - 1);
            }
            else
            {
                 _Type = Random.Range(0, m_enemyPrefabs.Length);
            }
           // Debug.Log(_Type);
            //Debug.Log(score.score);
            GameObject newobject = Instantiate(m_enemyPrefabs[_Type], m_spawnpoints[_randSpawPoint].position, transform.rotation);
            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
            //Debug.Log("Biasamode");
            yield return new WaitForSeconds(2.5f);
        }
            //StartCoroutine(e_spawnenemy());
     
     }

    public IEnumerator e_spawnenemyfrenzy()
    {
        while (true)
        {
           
            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length);
            int _Type = 0;
            //int _Type = Random.Range(0, m_enemyPrefabs.Length-1);
            if (score.score > 0 && score.score <= 3000)
            {
                _Type = Random.Range(0, m_enemyPrefabs.Length - 3);
            }
            else if (score.score > 3000 && score.score <= 5000)
            {
                _Type = Random.Range(0, m_enemyPrefabs.Length - 2);
            }
            else
            {
                _Type = Random.Range(0, m_enemyPrefabs.Length - 1);
            }
            //Debug.Log(_Type);
            GameObject newobject = Instantiate(m_enemyPrefabs[_Type], m_spawnpoints[_randSpawPoint].position, transform.rotation);
            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
            //Debug.Log("Edanmode");
            yield return new WaitForSeconds(0.5f);
        }
        //StartCoroutine(e_spawnenemyfrenzy());
    }
    public IEnumerator e_spawnmenu()
    {
        while (true)
        {
            int _randSpawPoint = Random.Range(0, m_spawnpoints.Length);
            int _Type = 0;
             _Type = Random.Range(0, m_enemyPrefabs.Length);
            //Debug.Log(_Type);
            //Debug.Log(score.score);
            GameObject newobject = Instantiate(m_enemyPrefabs[_Type], m_spawnpoints[_randSpawPoint].position, transform.rotation);
            newobject.transform.localScale = new Vector3(m_BesarObjek, m_BesarObjek, m_BesarObjek);
            //Debug.Log("Biasamode");
            yield return new WaitForSeconds(2.5f);
        }
    }
}
