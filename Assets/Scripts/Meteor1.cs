using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Meteor1 : MonoBehaviour
{
    public Transform m_bumi;
    public float m_fallSpeed = 1f;
    public float m_kecepatanrotasi = 10f;
    public GameObject meteorpecah;
    // Start is called before the first frame update
    void Start()
    {
        m_bumi = GameObject.Find("Earth").GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {
        string _namascenesaatini = SceneManager.GetActiveScene().name;
        if (_namascenesaatini == "Main")
        {
            if (PauseManager.pausemanager.isplaying)
            {
                transform.position = Vector3.MoveTowards(gameObject.transform.position, m_bumi.position, m_fallSpeed * Time.deltaTime);
                transform.Rotate(0f, 0f, m_kecepatanrotasi * Time.deltaTime, Space.Self);
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(gameObject.transform.position, m_bumi.position, m_fallSpeed * Time.deltaTime);
            transform.Rotate(0f, 0f, m_kecepatanrotasi * Time.deltaTime, Space.Self);
        }

    }
    public void pecah()
    {
        GameObject pecah1 = Instantiate(meteorpecah, transform.position, Quaternion.identity);
        GameObject pecah2 = Instantiate(meteorpecah, transform.position, Quaternion.identity);
        pecah1.transform.position += new Vector3(Random.Range(-.5f, .5f), Random.Range(-.5f, .5f), 0f);
        pecah2.transform.position += new Vector3(Random.Range(-.5f, .5f), Random.Range(-.5f, .5f), 0f);
        
    }
}
