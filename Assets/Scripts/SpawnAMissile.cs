using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAMissile : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject missile;
    public Transform player;


    // Update is called once per frame

    public void spawnmisil(Transform p_Target)
    {
        Vector3 DifferencePos = p_Target.transform.position - transform.position;
        float RotationZ = Mathf.Atan2(DifferencePos.y, DifferencePos.x) * Mathf.Rad2Deg;
        GameObject newobjek = Instantiate(missile, player.transform.position, Quaternion.identity);
        SoundManager.playsound("tembak");
        newobjek.GetComponent<HormingMissile>().target = p_Target;
        newobjek.transform.rotation = Quaternion.Euler(0, 0, RotationZ - 90f);

    }
}

//GetComponent<PlayermanagerScript>().transform.position