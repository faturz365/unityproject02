using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMusicScript : MonoBehaviour
{
    private static BackgroundMusicScript BGmusic;

    private void Awake()
    {
        if (BGmusic == null)
        {
            BGmusic = this;
            DontDestroyOnLoad(BGmusic);
        }
        else
        {
            Destroy(gameObject);
        }
    }


}

