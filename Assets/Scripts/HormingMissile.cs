using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HormingMissile : MonoBehaviour
{
	public Transform target;
	public GameObject ledakan;
	public float speed = 3f;
	public float rotateSpeed = 200f;
	private Rigidbody2D rb;
	public ScoreManager addingscore;
	public PlayermanagerScript m_player;
	public UpgradeManagerScipts m_upgrade;
	public int penguranganhp;
	//private int energy = 0;
	// Use this for initialization
	void Start()
	{
		rb = GetComponent<Rigidbody2D>();
		addingscore = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
		m_player = GameObject.Find("Player Manager").GetComponent<PlayermanagerScript>();
		m_upgrade = GameObject.Find("Upgrade Manager").GetComponent<UpgradeManagerScipts>();
		

	}

	void FixedUpdate()
	{
		if (PauseManager.pausemanager.isplaying)
		{
			Vector2 direction = (Vector2)target.position - rb.position;
			direction.Normalize();
			float rotateAmount = Vector3.Cross(direction, transform.up).z;
			rb.angularVelocity = -rotateSpeed * rotateAmount;
			rb.velocity = transform.up * speed;
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (PauseManager.pausemanager.isplaying)
		{

			if (col.tag == "Meteor" && col.transform == target.transform)
			{

				Destroy(gameObject);
				if (col.gameObject.GetComponent<Meteor1>() != null)
				{
					cekhomming(col.gameObject);
					Destroy(target.gameObject);
					SoundManager.playsound("bomb");
					m_upgrade.addmoney();
					col.gameObject.GetComponent<Meteor1>().pecah();
				}
				else if ((col.gameObject.GetComponent<NormalMeteor>().hp - penguranganhp) >= 1)
				{
					col.gameObject.GetComponent<NormalMeteor>().hp -= penguranganhp;
					GameObject duar = Instantiate(ledakan, transform.position, Quaternion.identity);
					duar.transform.localScale = new Vector3(1f, 1f, 1f);
					SoundManager.playsound("bomb");
					//m_upgrade.addmoney();

				}
				else
				{
					cekhomming(col.gameObject);
					Destroy(target.gameObject);
					SoundManager.playsound("bomb");
					Instantiate(ledakan, transform.position, Quaternion.identity);
					addingscore.tambahscore();
					m_player.forEngery();
					m_player.frenzymodebar();
					m_upgrade.addmoney();

				}
			}
		}
	}


	void cekhomming(GameObject targeting)
	{
		GameObject[] semuamissile = GameObject.FindGameObjectsWithTag("missile");
		foreach (GameObject current in semuamissile)
		{
			if (targeting.transform == current.GetComponent<HormingMissile>().target)
			{
				Destroy(current);
				Instantiate(ledakan, transform.position, Quaternion.identity);
			}
		}
	}
}