using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    public static PauseManager pausemanager;
    public bool isplaying = true;
    private bool pausebutt = false;
    public Button homebutt;
    // Start is called before the first frame update
    void Start()
    {
        pausemanager = this;
        pausebutt = false;
   
    }

    // Update is called once per frame
    void Update()
    {
        if (isplaying)
        {
            Time.timeScale = 1f;
        }
        else if (!isplaying)
        {
            Time.timeScale = 0f;
        }
    }

    public void pausebutton()
    {
        if (!pausebutt)
        {
            isplaying = false;
            pausebutt = true;
        }
        else if (pausebutt)
        {
            isplaying = true;
            pausebutt = false;
        }
        SoundManager.playsound("UIclick");
    }

    public void homebutton()
    {
        SoundManager.playsound("UIclick");
        isplaying = true;
        pausebutt = false;
        StartCoroutine(toohome());  
    }

    IEnumerator toohome()
    {
        
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene("Menu");
        StopCoroutine(toohome());
    }
}
