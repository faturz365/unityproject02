using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class NormalMeteor : MonoBehaviour
{
    public Transform m_bumi;
    public float m_fallSpeed = 1f;
    public float m_kecepatanrotasi = 10f;
    public int hp = 2;
    [SerializeField] private TextMeshProUGUI texthp;
    // Start is called before the first frame update
    void Start()
    {
        string _namascenesaatini = SceneManager.GetActiveScene().name;
        if (_namascenesaatini == "Main")
        {
            texthp.gameObject.SetActive(true);
        }
        else if (_namascenesaatini == "Menu")
        {
            texthp.gameObject.SetActive(false);
        }
        m_bumi = GameObject.Find("Earth").GetComponent<Transform>();
       
    }

    // Update is called once per frame
    void Update()
    {
        string namascenesaatini = SceneManager.GetActiveScene().name;
        if (namascenesaatini == "Main")
        {
            if (PauseManager.pausemanager.isplaying)
            {
                transform.position = Vector3.MoveTowards(gameObject.transform.position, m_bumi.position, m_fallSpeed * Time.deltaTime);
                transform.Rotate(0f, 0f, m_kecepatanrotasi * Time.deltaTime, Space.Self);
                texthp.text = hp.ToString();
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(gameObject.transform.position, m_bumi.position, m_fallSpeed * Time.deltaTime);
            transform.Rotate(0f, 0f, m_kecepatanrotasi * Time.deltaTime, Space.Self);
            
        }

    }
}
