using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class laserScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 0.75f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        gameObject.transform.Rotate(0f, 0f, 1000f * Time.deltaTime, Space.Self);
    }
}
