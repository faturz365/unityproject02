using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeManagerScipts : MonoBehaviour
{

    public int _money;
    private int _moneyforhp;
    private int _moneyformissilespeed;
    private int _moneyfordamage;
    private int _moneyforenergy;

    public PlayermanagerScript player;
    public HormingMissile horming;
    [SerializeField] Text hormingcost;
    [SerializeField] TextMeshProUGUI moneytext;
    [SerializeField] TextMeshProUGUI moneytext1;
    [SerializeField] Text hpcost;
    [SerializeField] Text damagecost;
    [SerializeField] Text energycost;
    [SerializeField] Button damagecostbutt;
    [SerializeField] Button energybutt;
    [SerializeField] Button Speedbutt;
    [SerializeField] Image homingcoin;
    [SerializeField] Image damegecoin;
    [SerializeField] Image energycoin;
    [SerializeField] Slider homingslider;
    [SerializeField] Slider damageslider;
    [SerializeField] Slider energyslider;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player Manager").GetComponent < PlayermanagerScript >();
        homingcoin.gameObject.SetActive(true);
        energycoin.gameObject.SetActive(true);
        damegecoin.gameObject.SetActive(true);
        _money = 0;
        _moneyforhp = 500;
        _moneyformissilespeed= 500;
        _moneyfordamage= 2000; 
        _moneyforenergy = 1000;
        horming.penguranganhp = 1;
        horming.speed = 2f;
        player.m_enegry = 2;
        homingslider.maxValue = 10;
        homingslider.minValue = horming.speed;

        damageslider.maxValue = 4;
        damageslider.minValue = horming.penguranganhp;

        energyslider.maxValue = 10;
        energyslider.minValue = player.m_enegry;
    }

    // Update is called once per frame
    void Update()
    {
       
        hpcost.text =  "  +1"+"\n" + " " + _moneyforhp;
        moneytext.text =  _money.ToString(); 
        moneytext1.text =  _money.ToString();
        if(horming.speed < 10)
        {
            hormingcost.text =    "Speed" + "\n" +" "+ _moneyformissilespeed;
        }
        else
        {
            hormingcost.text = "Max Upgrade";
            homingcoin.gameObject.SetActive(false);
        }

        if (player.m_enegry < 10)
        {
            energycost.text = " Energy " + "\n" +" " + _moneyforenergy;
        }
        else
        {
            energycost.text = "Max Upgrade";
            energycoin  .gameObject.SetActive(false);

        }

        if (horming.penguranganhp<4) 
        {
            damagecost.text = "Damage" + "\n" + " " + _moneyfordamage;

        }
        else
        {
            damagecost.text = "Max Upgrade";
           damegecoin.gameObject.SetActive(false);
        }

        homingslider.value = horming.speed;
        damageslider.value = horming.penguranganhp;
        energyslider.value = player.m_enegry;
    }

    public void addmoney()
    {
        _money += 50;
    }
    public void buyhp()
    {
        if (_money >= _moneyforhp)
        {
            player.m_hp += 1;
            _money -= _moneyforhp;
            player.textforhp.text = "HP Player :" + player.m_hp.ToString();

        }
        SoundManager.playsound("UIclick");
    }

    public void buyspeed()
    {
        if(_money >= _moneyformissilespeed)
        {
            horming.speed += 0.5f;
            _money -= _moneyformissilespeed;
            _moneyformissilespeed += 500;
            if (horming.speed >= 10) 
            {
                Speedbutt.interactable = false;
            }
        }
        SoundManager.playsound("UIclick");
    }

    public void buydamage()
    {
        if(_money >= _moneyfordamage)
        {
            horming.penguranganhp += 1;
            _money -= _moneyfordamage;
            _moneyfordamage += 3000;
            if(horming.penguranganhp >= 4)
            {
                damagecostbutt.interactable = false;
            }
        }
        SoundManager.playsound("UIclick");
    }

    public void buyenergy()
    {
        if (_money >= _moneyforenergy)
        {
            player.m_enegry += 1;
            _money -= _moneyforenergy;
            _moneyforenergy += 800; 
            if(player.m_enegry >= 10)
            {
                energybutt.interactable = false;
            }
        }
        SoundManager.playsound("UIclick");
    }

    public void upgrademenu()
    {
        if (PauseManager.pausemanager.isplaying)
        {
            PauseManager.pausemanager.isplaying = false;
        }
        else if (!PauseManager.pausemanager.isplaying)
        {
            PauseManager.pausemanager.isplaying = true;
        }
        SoundManager.playsound("UIclick");
    }

}
