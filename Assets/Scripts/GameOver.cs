using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{

    public TextMeshProUGUI textGameOver;
    public TextMeshProUGUI texthighscore;
    public TextMeshProUGUI textscore;
    public Button Bhome;
    public Button Bretry;
    float ukuran;

    // Start is called before the first frame update
    void Start()
    {
        textGameOver.gameObject.transform.localScale = new Vector3(0,0,0);
        texthighscore.gameObject.SetActive(false);
        textscore.gameObject.SetActive(false);
        Bhome.gameObject.SetActive(false);
        Bretry.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (textGameOver.gameObject.transform.localScale.x >= 1 || textGameOver.gameObject.transform.localScale.y >= 1 || textGameOver.gameObject.transform.localScale.z >= 1)
        {
            textGameOver.gameObject.transform.localScale = new Vector3(1, 1, 1);
            texthighscore.gameObject.SetActive(true);
            textscore.gameObject.SetActive(true);
            texthighscore.text = "HIGHSCORE" + "\n" + PlayerPrefs.GetInt("Highscore");
            textscore.text = "SCORE" + "\n" + PlayerPrefs.GetInt("Score");
            Bhome.gameObject.SetActive(true);
            Bretry.gameObject.SetActive(true);
        }

        else if(textGameOver.gameObject.transform.localScale.x < 1 || textGameOver.gameObject.transform.localScale.y < 1 || textGameOver.gameObject.transform.localScale.z < 1)
        {
            ukuran += Time.deltaTime;
            textGameOver.gameObject.transform.localScale = new Vector3(ukuran, ukuran, ukuran);
        }
    }
    
    public void homebutton()
    {
        SceneManager.LoadScene("Menu");
    }
    public void retrybutton()
    {
        SceneManager.LoadScene("Main");
    }

}
