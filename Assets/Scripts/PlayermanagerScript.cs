using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayermanagerScript : MonoBehaviour
{

    public ScoreManager m_score;
    public GameObject m_ledakan;
    public Transform m_bumi;
    public GameObject m_Laser;
    public int m_hp;
    private int _enegry;
    public int m_frenzybarmode;
    public int m_enegry;
    [SerializeField] private bool _frenzyactive = false;
    [SerializeField] private GameObject energybutton;
    [SerializeField] public TextMeshProUGUI textforhp;
    [SerializeField] public TextMeshProUGUI textforhp1;
    [SerializeField] private TextMeshProUGUI textforenergy;
    [SerializeField] private TextMeshProUGUI textfrezny;
    public SpawnAMissile spawnmissile;
    public KodinganUntukSpawn modemode;
    public Coroutine enemyfrenzy;
    public Coroutine enemyspawn;
    public Coroutine cautionstart;
    public CameraShaker cameraholder;
    public Slider energybarslider;
    public Gradient gradientforenergy;
    public Image energyfill;
    public Slider frenzybarslider;
    public Gradient gradientforfrenzy;
    public Image frenzyfill;
    public Image caution;
    public ScoreManager scoreman;
    private float cooldowntime;


    // Start is called before the first frame update
    void Start()
    {
        m_score = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        m_hp = 3;
        m_enegry = 2;

        cooldowntime = 0.5f;
        textforenergy.text = "Energy Laser :" + _enegry.ToString();
        textfrezny.text = "frenzy :" + m_frenzybarmode.ToString();
        enemyspawn = StartCoroutine(modemode.e_spawnenemy());
        energybarslider.maxValue = 100;
        energybarslider.minValue = 0;
        frenzybarslider.maxValue = 100;
        frenzybarslider.minValue = 0;
        gradientforenergy.Evaluate(1f);
        gradientforfrenzy.Evaluate(1f);

    }
    // Update is called once per frame
    void Update()
    {
        if (PauseManager.pausemanager.isplaying)
        {
            if (cooldowntime > 0)
            {
                cooldowntime -= Time.deltaTime;
            }
            if (SystemInfo.deviceType == DeviceType.Handheld)
            {
                if (Input.touchCount > 0 && cooldowntime <= 0)
                //if (Input.GetMouseButtonDown(0) && cooldowntime <= 0)
                {
                    cooldowntime = 0.5f;
                    Vector3 screenTouchPos = Input.GetTouch(0).position;
                    screenTouchPos.z = 0.5f;
                    RaycastHit2D tap = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(screenTouchPos), Vector2.zero);

                    //RaycastHit2D tap = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                    if (tap.collider != null)
                    {
                        if (tap.collider.gameObject.tag == "Meteor")
                        {
                            spawnmissile.spawnmisil(tap.transform);
                        }
                    }
                }
            }
            else
            {
           
                if (Input.GetMouseButtonDown(0) && cooldowntime <= 0)
                {
                    cooldowntime = 0.5f;
                    RaycastHit2D tap = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                    if (tap.collider != null)
                    {
                        if (tap.collider.gameObject.tag == "Meteor")
                        {
                            spawnmissile.spawnmisil(tap.transform);
                        }
                    }
                }
            }
            
                
        }

        if (_enegry >= 100)
        {
            if (PauseManager.pausemanager.isplaying)
            {
                energybutton.SetActive(true);
            }
            else
            {
                energybutton.SetActive(false);
            }
        }
        else if (_enegry < 100)
        {
            energybutton.SetActive(false);
        }
        textfrezny.text = "frenzy :" + m_frenzybarmode.ToString();
        textforhp.text = "X " + m_hp.ToString();
        textforhp1.text = "X " + m_hp.ToString();
        energybarslider.value = _enegry;
        energyfill.color = gradientforenergy.Evaluate(energybarslider.normalizedValue);
        frenzybarslider.value = m_frenzybarmode;
        frenzyfill.color = gradientforfrenzy.Evaluate(frenzybarslider.normalizedValue);
    }

    public void taplaser()
    {
        SoundManager.playsound("laser");
        Instantiate(m_Laser, m_bumi.transform.position, Quaternion.Euler(0f, 0f, 0f));
        Instantiate(m_Laser, m_bumi.transform.position, Quaternion.Euler(0f, 0f, 90f));
        Instantiate(m_Laser, m_bumi.transform.position, Quaternion.Euler(0f, 0f, 180f));
        Instantiate(m_Laser, m_bumi.transform.position, Quaternion.Euler(0f, 0f, 270f));
        var idduk = GameObject.FindGameObjectsWithTag("missile");
        var induk = GameObject.FindGameObjectsWithTag("Meteor");
        StartCoroutine(cameraholder.e_camerashake(0.8f, 0.1f));
        for (int i = 0; i < idduk.Length; i++)
        {
            Destroy(idduk[i]);
        }
        for (int i = 0; i < induk.Length; i++)
        {
            Destroy(induk[i], 0.2f);
            Instantiate(m_ledakan, induk[i].transform.position, Quaternion.identity);

            m_score.tambahscore();
        }
        _enegry = 0;
        textforenergy.text = "Energy Laser :" + _enegry.ToString();
    }

    public void subtractforhp()
    {
        if (m_hp > 1)
        {
            m_hp--;
        }
        else
        {
            SceneManager.LoadScene("EndGame");
        }
    }

    public void forEngery()
    {
        if (_enegry < 100)
        {
            _enegry += m_enegry;
        }
        else
        {
            _enegry = 100;
        }

        textforenergy.text = "Energy Laser :" + _enegry.ToString();
    }

    public void frenzymodebar()
    {
        if (m_frenzybarmode < 100)
        {
            if (scoreman.score >= 0 && scoreman.score <= 2000)
            {
                m_frenzybarmode += 2;
            }
            else if (scoreman.score > 2000 && scoreman.score <= 6000)
            {
                m_frenzybarmode += 4;
            }
            else if (scoreman.score > 6000 && scoreman.score <= 10000)
            {
                m_frenzybarmode += 6;
            }

        }
        else if (m_frenzybarmode >= 100)
        {
            m_frenzybarmode = 100;
        }

        frenzymode();
    }

    public void frenzymode()
    {
        if (m_frenzybarmode >= 100 && _frenzyactive == false)
        {
            StopAllCoroutines();
            StartCoroutine(e_frenzytime());
            StopCoroutine(enemyspawn);
            enemyfrenzy = StartCoroutine(modemode.e_spawnenemyfrenzy());
        }
    }

    void cekhomming(GameObject targeting)
    {
        GameObject[] semuamissile = GameObject.FindGameObjectsWithTag("missile");
        foreach (GameObject current in semuamissile)
        {
            if (targeting.transform == current.GetComponent<HormingMissile>().target)
            {
                Destroy(current);
                Instantiate(m_ledakan, transform.position, Quaternion.identity);
            }
        }
    }

    public IEnumerator e_frenzytime()
    {
        _frenzyactive = true;
        StartCoroutine(e_caution());
        yield return new WaitForSeconds(5f);
        _frenzyactive = false;
        caution.gameObject.SetActive(false);
        StopAllCoroutines();
        StopCoroutine(enemyfrenzy);
        StopCoroutine(enemyspawn);
        m_frenzybarmode = 0;
        enemyspawn = StartCoroutine(modemode.e_spawnenemy());
        yield return null;
    }

    public IEnumerator e_caution()
    {
        while (true)
        {
            caution.gameObject.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            caution.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.1f);
        }
    }
 
}
