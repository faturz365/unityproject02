using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionScripts : MonoBehaviour
{
    public GameObject m_ledakan;
    public PlayermanagerScript player;
    private void OnTriggerEnter2D(Collider2D col)
    {
        string _namascenesaatini = SceneManager.GetActiveScene().name;
        if (_namascenesaatini == "Main")
        {
            if (col.tag == "Meteor")
            {
                cekhomming(col.gameObject);
                Destroy(col.gameObject);
                Instantiate(m_ledakan, col.transform.position, Quaternion.identity);
                SoundManager.playsound("bomb");
                player.m_frenzybarmode = 0;
                player.subtractforhp();
            }
        }
        else
        {
            if (col.tag == "Meteor")
            {
                Destroy(col.gameObject);
                Instantiate(m_ledakan, col.transform.position, Quaternion.identity);
            }
        }
    }

    void cekhomming(GameObject targeting)
    {
        GameObject[] semuamissile = GameObject.FindGameObjectsWithTag("missile");
        foreach(GameObject current in semuamissile)
        {
            if(targeting.transform == current.GetComponent<HormingMissile>().target)
            {
                Destroy(current);
               // Debug.Log("ASEPSUPER");
                Instantiate(m_ledakan, current.transform.position, Quaternion.identity);
            }
        }

    }

}
